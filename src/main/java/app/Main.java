package app;

import app.rest.TestEndpoint;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;

import static javax.ws.rs.core.UriBuilder.fromUri;
import static org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory.createHttpServer;

public class Main {
    public static void main(String[] args) throws IOException {
        HttpServer server = startServer();
        System.in.read();
        server.shutdownNow();
    }

    private static HttpServer startServer () {
        return createHttpServer(fromUri("http://localhost/").port(8080).build(),
                new ResourceConfig(TestEndpoint.class));
    }
}
