package app.rest;

import app.rest.dto.UserDto;
import app.rest.entity.User;
import app.rest.mapper.UserMapper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("hello")
public class TestEndpoint {

    @GET
    @Produces(APPLICATION_JSON)
    public UserDto sayHello() {
        return new UserMapper().toDto(new User("first", "last"));
    }

}

