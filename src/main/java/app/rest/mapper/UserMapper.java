package app.rest.mapper;

import app.rest.dto.UserDto;
import app.rest.entity.User;

public class UserMapper extends DtoMapper<UserDto, User> {
    @Override protected Class<UserDto> dtoClass() { return UserDto.class; }
    @Override protected Class<User> entityClass() { return User.class; }
}
