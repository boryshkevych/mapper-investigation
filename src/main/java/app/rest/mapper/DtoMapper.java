package app.rest.mapper;

import com.googlecode.jmapper.JMapper;
import com.googlecode.jmapper.api.JMapperAPI;

import static com.googlecode.jmapper.api.JMapperAPI.global;
import static com.googlecode.jmapper.api.JMapperAPI.mappedClass;

public abstract class DtoMapper<D, E> {

    private final JMapper<D, E> mapper = createMapper(dtoClass(), entityClass());

    public D toDto(E s) { return mapper.getDestination(s); }

    private JMapper<D, E> createMapper(Class<D> dest, Class<E> src) {
        JMapperAPI api = new JMapperAPI().add(mappedClass(dest).add(global()));
        return new JMapper<>(dest, src, api);
    }

    protected abstract Class<D> dtoClass();
    protected abstract Class<E> entityClass();
}
